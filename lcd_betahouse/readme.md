# control lcd 20x4 with python script via gpio

development of python scripts that print customized info on the display  
to drive the lcd device I used the "the-raspberry-pi-guy" library (repository) --> [link github](https://github.com/the-raspberry-pi-guy/lcd) - [link YouTube](https://www.youtube.com/watch?v=fR5XhHYzUK0&t=7s)  

I found another python library --> [link github](https://github.com/carli2/raspberry-projects) - [link YouTube](https://www.youtube.com/watch?v=YUII39FzUb4)  

# developed scripts

## get_stoic

- use the service https://stoic.belelabestia.it/api/quotes to get the text to be shown on the display
- intelligently formats the text in pages of 4 lines x 20 characters showing them progressively without animations

![system_monitor_1](./media/stoic.gif)

---

## get_stoic_v_scroll

this script is an evolution of **get_stoic** with animations like *end credits*.  
it is possible to configure the initial and final scroll of the text and the scrolling speeds.

![system_monitor_1](./media/stoic-scroll.gif)


## system_monitor_1

it collects some information from the raspberry and shows it in a nice format

![system_monitor_1](./media/system_monitor_1.jpg)

## system_monito_2

it collects some information from the raspberry and shows it in a nice format

![system_monitor_1](./media/system_monitor_2.jpg)

# installazione

- run install.sh file. this will install python 2.7 and the libs needed to run the scripts
- run the script as a service:
  - create the file **/etc/systemd/system/lcd-betahouse.service**

    ```
    [Unit]
    Description=Servizio display info e altro su lcd
    After=multi-user.target

    [Service]
    Type=idle
    ExecStart=python /home/pi/lcd/lcd_betahouse.py
    Restart=always
    RestartSec=10
    User=pi

    [Install]
    WantedBy=multi-user.target
    ```

  - activate it
      ```bash
      $ sudo systemctl enable lcd-betahouse.service
      ```
  - start it
      ```bash
      $ sudo systemctl start lcd-betahouse.service
      $ sudo systemctl status lcd-betahouse.service
      $ sudo systemctl status lcd-betahouse.service
      ● lcd-betahouse.service - Servizio display info e altro su lcd
        Loaded: loaded (/etc/systemd/system/lcd-betahouse.service; enabled; vendor preset: enabled)
        Active: active (running) since Sun 2020-04-12 17:48:38 CEST; 2min 18s ago
      Main PID: 25172 (python)
          Tasks: 1 (limit: 2200)
        Memory: 6.0M
        CGroup: /system.slice/lcd-betahouse.service
                └─25172 /usr/bin/python /home/pi/lcd/lcd_betahouse.py

      Apr 12 17:48:38 raspberrypi systemd[1]: Started Servizio display info e altro su lcd.
      ```

# other useful links

- https://core-electronics.com.au/tutorials/stress-testing-your-raspberry-pi.html
- https://psutil.readthedocs.io/en/latest/#
- https://mkaz.blog/code/python-string-format-cookbook/