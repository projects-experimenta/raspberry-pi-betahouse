# -*- coding: utf-8 -*-
# import lcddriver
import urllib2
import time

# display = lcddriver.lcd()
url = 'https://stoic.belelabestia.it/api/quotes'
maxLineChar = 20
startRow = 3
endRow = 4
pause = 1
debug = True
try:
    while True:
        req = urllib2.Request(url)
        handler = urllib2.urlopen(req)
        text = handler.read()
        text = text\
            .replace("é", "e'")\
            .replace("è", "e'")\
            .replace("à", "a'")\
            .replace("ù", "u'")\
            .replace("ò", "o'")\
            .replace("ù", "u'")\
            .replace("ì", "i'")
        out = text.split()
        if debug:
            print
            print  handler.getcode()
            print  handler.headers.getheaders('content-type')
            print text

        toPrint = []
        i = 0
        row = startRow
        while i < len(out):
            if row >= endRow+1:
                if debug:
                    print '---------------------'
                row = startRow;
                time.sleep(pause)
                # display.lcd_clear()

            del toPrint[:]
            toPrint.append(out[i])
            i += 1
            full = False;
            while not full and i < len(out):
                if (len(''.join(toPrint)) + len(out[i]) + 1) <= maxLineChar:
                    toPrint.append(' ')
                    toPrint.append(out[i])
                    i += 1
                else:
                    full = True
            if debug:
                print str(row) + ': ' + ''.join(toPrint)
            # display.lcd_display_string(''.join(toPrint), row)
            row += 1
        time.sleep(pause)
        # display.lcd_clear()
except KeyboardInterrupt:  # If there is a KeyboardInterrupt (when you press ctrl+c), exit the program and c$
    if not debug:
        print("Cleaning up!")
        display.lcd_clear()
