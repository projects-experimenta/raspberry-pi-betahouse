# -*- coding: utf-8 -*-
# import lcddriver
import urllib2
import time

# display = lcddriver.lcd()
url = 'https://stoic.belelabestia.it/api/quotes'
maxLineChar = 20
fastScroll = 1
waitScroll = 5
scrollIn = True
scrollOut = True
debug = True

try:
    while True:
        req = urllib2.Request(url)
        handler = urllib2.urlopen(req)
        text = handler.read()
        text = text\
            .replace("é", "e'")\
            .replace("è", "e'")\
            .replace("à", "a'")\
            .replace("ù", "u'")\
            .replace("ò", "o'")\
            .replace("ù", "u'")\
            .replace("ì", "i'")
        out = text.split()

        if debug:
            print
            print handler.getcode()
            print handler.headers.getheaders('content-type')
            print text

        i = 0
        row = []
        textRows = []
        if scrollIn:
            textRows.append('')
            textRows.append('')
            textRows.append('')
            textRows.append('')
            timeScroll = fastScroll
        else:
            timeScroll = waitScroll

        textRows.append('*stoic.belelabestia*')
        textRows.append('')
        while i < len(out):
            row.append(out[i])
            i += 1
            full = False;
            while not full and i < len(out):
                if (len(''.join(row)) + len(out[i]) + 1) <= maxLineChar:
                    row.append(' ')
                    row.append(out[i])
                    i += 1
                else:
                    full = True
            textRows.append(''.join(row))
            del row[:]

        offset = 3
        if scrollOut:
            textRows.append('')
            textRows.append('')
            textRows.append('')
            textRows.append('')
            offset += 4

        i = 0
        l = len(textRows)
        while i < l-offset:
            if debug:
                print(chr(27) + "[2J")
                print textRows[i]
                print textRows[i+1]
                print textRows[i+2]
                print textRows[i+3]
            # display.lcd_clear()
            # display.lcd_display_string(textRows[i], 1)
            # display.lcd_display_string(textRows[i+1], 2)
            # display.lcd_display_string(textRows[i+2], 3)
            # display.lcd_display_string(textRows[i+3], 4)
            if l-4 != i:
                time.sleep(timeScroll)
            i+=1
            if i % 4 != 0:
                timeScroll = fastScroll
            else:
                timeScroll = waitScroll

        time.sleep(waitScroll)
        if scrollOut:
            for k in range(i,l-3):
                if debug:
                    print(chr(27) + "[2J")
                    print textRows[k]
                    print textRows[k + 1]
                    print textRows[k + 2]
                    print textRows[k + 3]
                # display.lcd_clear()
                # display.lcd_display_string(textRows[k], 1)
                # display.lcd_display_string(textRows[k+1], 2)
                # display.lcd_display_string(textRows[k+2], 3)
                # display.lcd_display_string(textRows[k+3], 4)
                time.sleep(fastScroll)
except KeyboardInterrupt:  # If there is a KeyboardInterrupt (when you press ctrl+c), exit the program and c$
    if not debug:
        print("Cleaning up!")
        display.lcd_clear()
