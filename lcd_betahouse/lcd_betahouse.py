# -*- coding: utf-8 -*-
import lcddriver
import time
import psutil
import urllib2
import json
import datetime

def get_bytes(t, iface='wlan0'):
    with open('/sys/class/net/' + iface + '/statistics/' + t + '_bytes', 'r') as f:
        data = f.read();
        return int(data)

def get_cpu_times():
    # Read first line from /proc/stat. It should start with "cpu"
    # and contains times spend in various modes by all CPU's totalled.
    #
    with open("/proc/stat") as procfile:
        cpustats = procfile.readline().split()

    # Sanity check
    #
    if cpustats[0] != 'cpu':
        raise ValueError("First line of /proc/stat not recognised")

    # Refer to "man 5 proc" (search for /proc/stat) for information
    # about which field means what.
    #
    # Here we do calculation as simple as possible:
    #
    # CPU% = 100 * time-doing-things / (time_doing_things + time_doing_nothing)
    #

    user_time = int(cpustats[1])  # time spent in user space
    nice_time = int(cpustats[2])  # 'nice' time spent in user space
    system_time = int(cpustats[3])  # time spent in kernel space

    idle_time = int(cpustats[4])  # time spent idly
    iowait_time = int(cpustats[5])  # time spent waiting is also doing nothing

    time_doing_things = user_time + nice_time + system_time
    time_doing_nothing = idle_time + iowait_time

    return time_doing_things, time_doing_nothing

def system_monitor_1(repeat_number=5, loop_delay=1):
    prev_time_doing_things = 0
    prev_time_doing_nothing = 0
    counter = 0
    display.lcd_clear()

    # display title
    #
    titolo = "* RASPI  BETAHOUSE *"
    display.lcd_display_string(titolo, 1)

    while counter < repeat_number:  # loop forever printing system usage percentage
        try:
            time_doing_things, time_doing_nothing = get_cpu_times()
            diff_time_doing_things = time_doing_things - prev_time_doing_things
            diff_time_doing_nothing = time_doing_nothing - prev_time_doing_nothing
            cpu_percentage = 100.0 * diff_time_doing_things / (diff_time_doing_things + diff_time_doing_nothing)
            if cpu_percentage == 100:
                cpu_percentage_row = "CPU: ", cpu_percentage, "%"
            else:
                cpu_percentage_row = "CPU: {:0>4.1f}%".format(cpu_percentage)

            # remember current values to subtract next iteration of the loop
            #
            prev_time_doing_things = time_doing_things
            prev_time_doing_nothing = time_doing_nothing
        except:
            cpu_percentage_row = "CPU:error"

        # memory usage
        #
        try:
            memory_use = psutil.virtual_memory()[2]
            if memory_use == 100:
                memory_use_row = "RAM: 100%"
            else:
                memory_use_row = "RAM: {:0>4.1f}%".format(memory_use)
        except:
            memory_use_row = "RAM:error"

        # swap usage
        #
        try:
            swap_use = psutil.swap_memory()[3]
            if swap_use == 100:
                swap_use_row = "SWAP:100%"
            else:
                swap_use_row = "SWAP:{:0>4.1f}%".format(swap_use)
        except:
            swap_use_row = "SWAP:error"


        # sensors_temperatures
        #
        try:
            sensors_temperatures_row = "C: {}".format(psutil.sensors_temperatures(fahrenheit=False)['cpu-thermal'][0][1])
        except:
            sensors_temperatures_row ="C:error"

        # monitoring network speed
        #
        try:
            tx1 = get_bytes('tx', iface='eth0')
            rx1 = get_bytes('rx', iface='eth0')

            time.sleep(1)

            tx2 = get_bytes('tx', iface='eth0')
            rx2 = get_bytes('rx', iface='eth0')

            # tx_speed = round((tx2 - tx1)/1000000.0, 4)
            # rx_speed = round((rx2 - rx1)/1000000.0, 4)
            tx_speed = ((tx2 - tx1) / 1000000.0) * 8
            rx_speed = ((rx2 - rx1) / 1000000.0) * 8

            network_speed_row = "DL:{:0>6.2f} UL:{:0>6.2f}".format(tx_speed, rx_speed)
        except:
            network_speed_row = "DL/UL: error"

        # display info
        #
        if debug:
            print "-- SYSTEM MONITOR 1 --"
            print titolo
            print cpu_percentage_row
            print memory_use_row
            print swap_use_row
            print sensors_temperatures_row
            print network_speed_row

        display.lcd_display_string("{} {}".format(cpu_percentage_row, memory_use_row), 2)
        display.lcd_display_string("{} {}".format(swap_use_row, sensors_temperatures_row), 3)
        display.lcd_display_string(network_speed_row, 4)

        # Loop delay
        #
        time.sleep(loop_delay)
        counter += 1

def get_public_ip():
    try:
        public_ip_address = urllib2.urlopen("https://api.ipify.org?format=json").read()
        return json.loads(public_ip_address)["ip"]
    except:
        print "Errore nell'ottenere l'ip pubblico"
        return "ip !risolto"

def system_monitor_2(repeat_number=5, loop_delay=1, public_ip_address = ""):
    counter = 0
    display.lcd_clear()

    # display title
    #
    titolo = "* RASPI  BETAHOUSE *"
    display.lcd_display_string(titolo, 1)

    while counter < repeat_number:  # loop forever printing system usage percentage
        # hard disk info
        #
        try:
            hdd1 = "{}%".format(psutil.disk_usage('/media/hdd-750')[3])
        except:
            hdd1 = "error"
        try:
            hdd2 = "{}%".format(psutil.disk_usage('/media/hdd-500')[3])
        except:
            hdd2 = "error"
        hdd_row = "HD1:{} HD2:{}".format(hdd1, hdd2)

        # IP Address URL
        #
        if public_ip_address == "":
            public_ip_row = "IP: {}".format(get_public_ip())
        else:
            public_ip_row = "IP: {}".format(public_ip_address)

        # Load Avg
        #
        try:
            avg = [x / psutil.cpu_count() * 100 for x in psutil.getloadavg()]
            if avg[0] >= 100:
                avg[0] = "{:0f}".format(avg[0])
            if avg[1] >= 100:
                avg[1] = "{:0f}".format(avg[1])
            if avg[2] >= 100:
                avg[2] = "{:0f}".format(avg[2])
            avg_row = "AVG:{:0>4.1f}, {:0>4.1f}, {:0>4.1f}".format(avg[0], avg[1], avg[2])
        except:
            avg_row = "AVG: error"

        # display info
        #
        if debug:
            print "-- SYSTEM MONITOR 2 --"
            print titolo
            print hdd_row
            print public_ip_row
            print avg_row

        display.lcd_display_string(hdd_row, 2)
        display.lcd_display_string(public_ip_row, 3)
        display.lcd_display_string(avg_row, 4)

        # Loop delay
        #
        time.sleep(loop_delay)
        counter += 1

def stoic_belelabestia(number_of_sentences=1, loop_delay=3):
    counter = 0
    url = 'https://stoic.belelabestia.it/api/quotes'
    maxLineChar = 20
    startRow = 1
    endRow = 4
    display.lcd_clear()

    while counter < number_of_sentences:
        try:
            req = urllib2.Request(url)
            handler = urllib2.urlopen(req)
            text = handler.read()
            text = text \
                .replace("é", "e'") \
                .replace("è", "e'") \
                .replace("à", "a'") \
                .replace("ù", "u'") \
                .replace("ò", "o'") \
                .replace("ù", "u'") \
                .replace("ì", "i'")
            out = text.split()
            out = ['*stoic.belelabestia*'] + [' '*20] + text.split()
        except:
            out = ['*stoic.belelabestia*'] + [' '*20] + ['error']

        if debug:
            print
            print  handler.getcode()
            print  handler.headers.getheaders('content-type')
            print text

        toPrint = []
        i = 0
        row = startRow
        while i < len(out):
            if row >= endRow + 1:
                if debug:
                    print '---------------------'
                row = startRow;
                time.sleep(loop_delay)
                display.lcd_clear()

            del toPrint[:]
            toPrint.append(out[i])
            i += 1
            full = False;
            while not full and i < len(out):
                if (len(''.join(toPrint)) + len(out[i]) + 1) <= maxLineChar:
                    toPrint.append(' ')
                    toPrint.append(out[i])
                    i += 1
                else:
                    full = True
            if debug:
                print str(row) + ': ' + ''.join(toPrint)
            display.lcd_display_string(''.join(toPrint), row)
            row += 1
        time.sleep(loop_delay)
        counter += 1
    time.sleep(1)

# ------------------------- MAIN -------------------------------
try:
    display = lcddriver.lcd()
    debug = True
    startDate = datetime.datetime.now()
    ip = get_public_ip()

    while True:
        system_monitor_1(repeat_number=15, loop_delay=4)
        system_monitor_2(public_ip_address=ip, repeat_number=12, loop_delay=5)
        stoic_belelabestia()

        minutes = (datetime.datetime.now()-startDate).seconds / 60
        if (minutes > 120):
            ip = get_public_ip()
except KeyboardInterrupt:  # If there is a KeyboardInterrupt (when you press ctrl+c), exit the program and c$
    if not debug:
        display.lcd_display_string("  CIAOOOOOOOOOO!!!  ", 3)
        print("Cleaning up!")
        display.lcd_clear()
