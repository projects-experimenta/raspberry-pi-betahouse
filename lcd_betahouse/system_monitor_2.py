# -*- coding: utf-8 -*-
import lcddriver
import time
import psutil
import urllib2
import json

display = lcddriver.lcd()
loop_delay = 1
debug = True

# display title
#
titolo = "* RASPI  BETAHOUSE *"
display.lcd_display_string(titolo, 1)
try:
    while True:  # loop forever printing system usage percentage
        # hard disk info
        #
        try:
            hdd1 = "{}%".format(psutil.disk_usage('/media/hdd-750')[3])
        except:
            hdd1 = "error"
        try:
            hdd2 = "{}%".format(psutil.disk_usage('/media/hdd-500')[3])
        except:
            hdd2 = "error"
        hdd_row = "HD1:{} HD2:{}".format(hdd1, hdd2)

        # IP Address URL
        #
        public_ip_address = urllib2.urlopen("https://api.ipify.org?format=json").read()
        public_ip_row = "IP: {}".format(json.loads(public_ip_address)["ip"])

        # Load Avg
        #
        try:
            avg = [x / psutil.cpu_count() * 100 for x in psutil.getloadavg()]
            if avg[0] == 100:
                avg[0] = "100"
            if avg[1] == 100:
                avg[1] = "100"
            if avg[2] == 100:
                avg[2] = "100"
            avd_row = "AVG:{:0>4.1f}, {:0>4.1f}, {:0>4.1f}".format(avg[0], avg[1], avg[2])
        except:
            avg_row = "AVG: error"

        # display info
        #
        if debug:
            print "-- SYSTEM MONITOR 2 --"
            print titolo
            print hdd_row
            print public_ip_row
            print avd_row

        display.lcd_display_string(hdd_row, 2)
        display.lcd_display_string(public_ip_row, 3)
        display.lcd_display_string(avd_row, 4)

        # Loop delay
        #
        time.sleep(loop_delay)
except KeyboardInterrupt:  # If there is a KeyboardInterrupt (when you press ctrl+c), exit the program and c$
    if not debug:
        print("Cleaning up!")
        display.lcd_display_string("  CIAOOOOOOOOOO!!!  ", 3)
        display.lcd_clear()
