# -*- coding: utf-8 -*-
import lcddriver
import time
import psutil
import socket

def get_bytes(t, iface='wlan0'):
    with open('/sys/class/net/' + iface + '/statistics/' + t + '_bytes', 'r') as f:
        data = f.read();
        return int(data)

def get_cpu_times():
    # Read first line from /proc/stat. It should start with "cpu"
    # and contains times spend in various modes by all CPU's totalled.
    #
    with open("/proc/stat") as procfile:
        cpustats = procfile.readline().split()

    # Sanity check
    #
    if cpustats[0] != 'cpu':
        raise ValueError("First line of /proc/stat not recognised")

    # Refer to "man 5 proc" (search for /proc/stat) for information
    # about which field means what.
    #
    # Here we do calculation as simple as possible:
    #
    # CPU% = 100 * time-doing-things / (time_doing_things + time_doing_nothing)
    #

    user_time = int(cpustats[1])  # time spent in user space
    nice_time = int(cpustats[2])  # 'nice' time spent in user space
    system_time = int(cpustats[3])  # time spent in kernel space

    idle_time = int(cpustats[4])  # time spent idly
    iowait_time = int(cpustats[5])  # time spent waiting is also doing nothing

    time_doing_things = user_time + nice_time + system_time
    time_doing_nothing = idle_time + iowait_time

    return time_doing_things, time_doing_nothing

display = lcddriver.lcd()
loop_delay = 1
debug = True

# display title
#
titolo = "* RASPI  BETAHOUSE *"
display.lcd_display_string(titolo, 1)
try:
    prev_time_doing_things = 0
    prev_time_doing_nothing = 0
    while True:  # loop forever printing system usage percentage
        try:
            time_doing_things, time_doing_nothing = get_cpu_times()
            diff_time_doing_things = time_doing_things - prev_time_doing_things
            diff_time_doing_nothing = time_doing_nothing - prev_time_doing_nothing
            cpu_percentage = 100.0 * diff_time_doing_things / (diff_time_doing_things + diff_time_doing_nothing)
            if cpu_percentage == 100:
                cpu_percentage_row = "CPU: ", cpu_percentage, "%"
            else:
                cpu_percentage_row = "CPU: {:0>4.1f}%".format(cpu_percentage)

            # remember current values to subtract next iteration of the loop
            #
            prev_time_doing_things = time_doing_things
            prev_time_doing_nothing = time_doing_nothing
        except:
            cpu_percentage_row = "CPU:error"

        # memory usage
        #
        try:
            memory_use = psutil.virtual_memory()[2]
            if memory_use == 100:
                memory_use_row = "RAM: 100%"
            else:
                memory_use_row = "RAM: {:0>4.1f}%".format(memory_use)
            if debug:
                print memory_use_row
        except:
            memory_use_row = "RAM:error"

        # swap usage
        #
        try:
            swap_use = psutil.swap_memory()[3]
            if swap_use == 100:
                swap_use_row = "SWAP:100%"
            else:
                swap_use_row = "SWAP:{:0>4.1f}%".format(swap_use)
        except:
            swap_use_row = "SWAP:error"

        # sensors_temperatures
        #
        try:
            sensors_temperatures_row = "C: {}".format(psutil.sensors_temperatures(fahrenheit=False)['cpu-thermal'][0][1])
        except:
            sensors_temperatures_row ="C:error"

        # monitoring network speed
        #
        try:
            tx1 = get_bytes('tx')
            rx1 = get_bytes('rx')

            time.sleep(1)

            tx2 = get_bytes('tx')
            rx2 = get_bytes('rx')

            # tx_speed = round((tx2 - tx1)/1000000.0, 4)
            # rx_speed = round((rx2 - rx1)/1000000.0, 4)
            tx_speed = ((tx2 - tx1) / 1000000.0) * 8
            rx_speed = ((rx2 - rx1) / 1000000.0) * 8

            network_speed_row = "DL:{:0>6.2f} UL:{:0>6.2f}".format(tx_speed, rx_speed)
        except:
            network_speed_row = "DL/UL: error"

        # display info
        #
        if debug:
            print "-- SYSTEM MONITOR 1 --"
            print titolo
            print cpu_percentage_row
            print memory_use_row
            print swap_use_row
            print sensors_temperatures_row
            print network_speed_row

        display.lcd_display_string("{} {}".format(cpu_percentage_row, memory_use_row), 2)
        display.lcd_display_string("{} {}".format(swap_use_row, sensors_temperatures_row), 3)
        display.lcd_display_string(network_speed_row, 4)

        # Loop delay
        #
        time.sleep(loop_delay)
except KeyboardInterrupt:  # If there is a KeyboardInterrupt (when you press ctrl+c), exit the program and c$
    if not debug:
        display.lcd_display_string("  CIAOOOOOOOOOO!!!  ", 3)
        print("Cleaning up!")
        display.lcd_clear()
